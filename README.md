# Ejercicio para completar aplicación Java Web de ECommerce
 
## Objetivo
 
Tomando como base las clases e interfaces de este repositorio, junto con los archivos .csv para importar a una nueva Base de Datos:

Realizar una aplicación web en la cual un usuario deba realizar el login para poder realizar determinadas acciones.

El login debe realizarse contra una Base de Datos MySql. Asumir que los usuarios ya se registraron por otros medios y existen en la Base de Datos.

El usuario podrá ver un listado de productos. (No se requiere login)

Podrá elegir ver productos por categoría. (No se requiere login)

Podrá acceder al detalle del producto al clickear sobre el mismo. (No se requiere login)

A su vez, cada producto podrá Agregarlo al carrito de compras o eliminarlo si ya existe. (Login requerido)

Podrá acceder al carrito de compras y y ver los productos del carrito junto con la cantidad de cada uno y el precio total de los mismos. (Login requerido)
Sólo podrá haber 1 carrito de compras pendiente a la vez.

El carrito se debe guardar en la sesión del usuario. por lo que si sale y vuelve a ingresar deberá seguir viendo el carrito con sus productos.

Podrá confirmar la compra y la misma se confirmará en la base de datos y se le mostrará por pantalla el detalle y el total de la compra.
 
Además, el usuario podrá ver las compras finalizadas pero no podrá modificarlas. (Login requerido)

## Bonus
Programar la registración de usuarios.

Programar el Backoffice de la aplicación.
Es decir, con un usuario Administrador, poder dar de alta, eliminar y modificar productos via web.